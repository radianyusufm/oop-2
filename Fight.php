<?php

require_once 'Hewan.php';

class Fight extends Hewan{
  protected $atackPower;
   protected $defencePower;
   private $penyerang;

   public function serang(){
     if ($this->nama == 'elang') {
       $this->penyerang = 'harimau';
     } else if ($this->nama == 'harimau'){
       $this->penyerang = 'elang';
     }
     return $this->nama . ' sedang menyerang ' . $this->penyerang;
   }

   public function diserang(){
     if ($this->nama == 'elang') {
       $this->darah = ( $this->darah - 7) / 5;
     } else if ($this->nama == 'harimau'){
       $this->darah = ($this->darah - 10 )/ 8;
     }
     return $this->nama . ' sedang diserang ' . $this->penyerang . ' sisa darah sekarang : ' . $this->darah ;
   }

}

 ?>
