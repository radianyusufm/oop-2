<?php
require_once 'Fight.php';

class harimau extends Fight {
  public function __CONSTRUCT($nama, $jumlahKaki, $keahlian, $atackPower, $defencePower){
    $this->nama = $nama;
    $this->jumlahKaki = $jumlahKaki;
    $this->keahlian = $keahlian;
    $this->atackPower = $atackPower;
    $this->defencePower = $defencePower;
  }

  public function getInfoHewan(){
    return 'Info Hewan <br> '.
            'nama : '.$this->nama. '<br>'.
            'Jumlah Kaki : '. $this->jumlahKaki. '<br>'.
            'Darah : ' . $this->darah. '<br>'.
            'Keahlian : '.$this->keahlian. '<br>'.
            'Attack Power :'.$this->atackPower. '<br>'.
            'devence Power : '.$this->defencePower. '<br>';

  }

}

$harimau = new Harimau('harimau', 4, 'berlari cepat', 7, 8);
echo $harimau->getInfoHewan();
echo "<hr>";
echo $harimau->atraksi();
echo "<hr>";
echo $harimau->serang();
echo "<hr>";
echo $harimau->diserang();
echo "<hr>";

 ?>
